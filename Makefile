NAME := sortpics

.PHONY: check-pyenv
check-pyenv:
	@echo
	@ if ! hash pyenv 1>/dev/null 2>&1; then \
			echo "ERROR! pyenv missing." >&2 ;\
			exit 1 ;\
		fi

.PHONY: check-poetry
check-poetry:
	@echo
	@ if ! hash poetry 1>/dev/null 2>&1; then \
			echo "ERROR! poetry missing." >&2 ;\
			exit 1 ;\
		fi


################################################################################
.PHONY: setup
setup: setup-python setup-python-modules

.PHONY: setup-python
setup-python: check-pyenv
	@echo
	pyenv install --skip-existing $(shell cat .python-version)
	pyenv rehash

.PHONY: setup-python-modules
setup-python-modules: check-poetry
	@echo
	poetry install


################################################################################
.PHONY: update
update: update-python update-python-modules

.PHONY: update-python
update-python: check-python
	@echo
	pyenv local $(VERSION)
	sed -i 's/python:.*/python:$(VERSION)/g' .gitlab-ci.yml

.PHONY: update-python-modules
update-python-modules: check-poetry
	@echo
	poetry update



################################################################################
.PHONY: fixup
fixup: fixup-python

.PHONY: fixup-python
fixup-python:
	@echo
	poetry run isort .
	poetry run black .


.PHONY: clean
clean:
	rm -rf .pytest_cache dist

################################################################################
lint: lint-whitespace lint-black lint-flake8 lint-isort lint-darglint lint-pydocstyle

.PHONY: lint-whitespace
lint-whitespace:
	@echo
	git diff --check HEAD --

.PHONY: lint-black
lint-black:
	@echo
	poetry run black --check .

.PHONY: lint-flake8
lint-flake8:
	@echo
	poetry run flake8 --benchmark

.PHONY: lint-isort
lint-isort:
	@echo
	poetry run isort --check --diff .

.PHONY: lint-darglint
lint-darglint:
	@echo
	poetry run darglint

.PHONY: lint-pydocstyle
lint-pydocstyle:
	@echo
	poetry run pydocstyle


################################################################################
test: unit-pytest

.PHONY: unit-pytest
unit-pytest:
	@echo
	poetry run pytest


################################################################################
.PHONY: build
build:
	@echo
	poetry build

.PHONY: docs
docs:
	@echo
	poetry run pdoc --force --html --output-dir docs $(NAME)
	poetry run pdoc --force --output-dir docs $(NAME)


################################################################################
.PHONY: release
release: tag

.PHONY: tag
tag: check-git
tag:
	@echo
	@echo Tagging git ...
	git tag -am "Tagging release $(VERSION)" v$(VERSION)
	@ if [ -n "$(shell git remote -v)" ] ; then git push --tags ; else echo 'no remote to push tags to' ; fi

.PHONY: check-git
check-git: check-git-dirty check-git-branch-master

.PHONY: check-git-dirty
check-git-dirty:
	@ if [[ -n "$(shell git status -s .)" ]]; then echo "ERROR: working directory contains modified/untracked files.  Please commit or discard changes first." >&2; exit 1; fi

.PHONY: check-git-branch-master
check-git-branch:
	@ if [[ "$(shell git rev-parse --abbrev-ref HEAD)" == "main" ]]; then echo "ERROR: not on release branch." >&2; exit 1; fi

################################################################################
.PHONY: publish
publish: poetry-publish

.PHONY: poetry-publish
poetry-publish:
	@ echo
	poetry publish
