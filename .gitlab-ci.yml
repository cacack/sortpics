---
stages:
  - lint
  - test
  - build

variables:
  PYTHON_VERSION: "3.8.10"
  POETRY_VERSION: "1.1.6"
  ALPINE_VERSION: "3.13"
  PYTHON_IMAGE: "python:${PYTHON_VERSION}"
  ALPINE_IMAGE: "alpine:${ALPINE_VERSION}"

# Reusable chunks of code
.bootstrap_poetry: &bootstrap_poetry |
  pip install "poetry==$POETRY_VERSION"

.alpine_setup: &alpine_setup |
  apk update
  apk add \
    bash \
    coreutils \
    curl \
    findutils \
    git \
    grep \
    make

.setup_ssh_agent: &setup_ssh_agent |
  which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )
  eval $(ssh-agent -s)
  echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh

check_for_whitespace_errors:
  stage: lint
  image: $ALPINE_IMAGE
  script:
    - *alpine_setup
    - make lint-whitespace

flake8:
  stage: lint
  image: $PYTHON_IMAGE
  script:
    - *bootstrap_poetry
    - make setup-python-modules
    - make lint-flake8

black:
  stage: lint
  image: $PYTHON_IMAGE
  script:
    - *bootstrap_poetry
    - make setup-python-modules
    - make lint-black

isort:
  stage: lint
  image: $PYTHON_IMAGE
  script:
    - *bootstrap_poetry
    - make setup-python-modules
    - make lint-isort .

darglint:
  stage: lint
  image: $PYTHON_IMAGE
  script:
    - *bootstrap_poetry
    - make setup-python-modules
    - make lint-darglint

################################################################################
pytest:
  stage: test
  image: $PYTHON_IMAGE
  script:
    - *bootstrap_poetry
    - make setup-python-modules
    - make unit-pytest
  coverage: "/TOTAL.+ ([0-9]{1,3}%)/"

################################################################################
build:
  stage: build
  image: $PYTHON_IMAGE
  script:
    - *bootstrap_poetry
    - make setup-python-modules
    - make build
