import errno
import hashlib
import logging
import pathlib
from datetime import datetime, timedelta
import dateutil.parser
import json
import os
import re
import uuid
import shutil
import tempfile

import exiftool

VALID_EXTENSIONS = [
    "cr2",
    "crw",
    "dcr",
    "dng",
    "jpg",
    "jpeg",
    "mov",
    "mp4",
    "nef",
    "png",
    "raw",
    "tiff",
]

RAW_EXTENSIONS = [
    "arw",  # Sony
    "crw",  # Canon
    "cr2",  # Canon
    "dng",  # Adobe, Leica
    "mrw",  # Minolta
    "nef",  # Nikon
    "nrw",  # Nikon
    "orf",  # Olympus
    "pef",  # Pentax
    "ptx",  # Pentax
    "raw",  # Panasonic, Leica
    "rw2",  # Panasonic
    "rwl",  # Leica
    "srf",  # Sony
    "sr2",  # Sony
    "srw",  # Samsung
    "x3f",  # Sigma
]

# DATE_PATTERN = re.compile("([0-9]{8})-?([0-9]{6})?(\.[0-9]+)")
DATE_PATTERN = re.compile(r"([0-9]{8})(.)?([0-9]{6})?(.)?([0-9]+)?")


class ExifNotFound(Exception):
    pass

class ImageRename:
    def __init__(self, source_filename, destination_base_dir, old_naming=False, raw_path=None, move=False, precision=6, dry_run=None, time_adjust=None, day_adjust=None):
        # Define logger
        self.log = logging.getLogger(__name__)

        self.precision = precision
        self.is_move = move
        self.old_naming = old_naming
        self.dry_run = dry_run
        self.source_unresolved = pathlib.Path(source_filename).expanduser()
        self.source = self.source_unresolved.resolve()
        self.extension = self.source.suffix.replace(".", "")

        if time_adjust:
            self.time_delta = calculate_time_delta(time_adjust)
        else:
            self.time_delta = None

        if day_adjust:
            self.day_delta = calculate_day_delta(day_adjust)
        else:
            self.day_delta = None

        if self.is_raw() and raw_path:
            self.destination_base = pathlib.Path(raw_path).expanduser().resolve()
        else:
            self.destination_base = pathlib.Path(destination_base_dir).expanduser().resolve()
        self.supported_extensions = VALID_EXTENSIONS

        self.raw_metadata = None

    def _calculate_sha256(self, filename):
        the_hash = hashlib.sha256()
        with open(filename, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                the_hash.update(chunk)

        return the_hash.hexdigest()

    def _generate_destination(self, increment=None):
        try:
            if self.datetime.microsecond:
                # self.log.debug(f"datetime.microsecond: {self.datetime.microsecond}")
                # subsec_length = len(str(self.datetime.microsecond)) - self.precision
                # self.log.debug(f"datetime.microsecond length: {len(str(subsec_length))}")
                # self.log.debug(f"subsec_length: {subsec_length}")
                # subsec = self.datetime.strftime("%f")[:-(subsec_length)]
                subsec = self.datetime.strftime("%f")[: (self.precision)]
                self.log.debug(f"subsec: {subsec}")
            else:
                subsec = "0" * self.precision
        except AttributeError:
            subsec = "0" * self.precision

        if increment:
            increment_string = f"_{increment}"
        else:
            increment_string = ""

        if self.make and self.model:
            if self.old_naming:
                camera = f"{self.make}{self.model}"
            else:
                camera = f"{self.make}-{self.model}"
        elif self.make:
            camera = self.make
        elif self.model:
            camera = self.model

        try:
            destination_dir = self.datetime.strftime("%Y/%m/%Y-%m-%d")
        except AttributeError:
            destination_dir = "unknown"
        try:
            filename_dt = self.datetime.strftime("%Y%m%d-%H%M%S")
            destination_filename = f"{filename_dt}.{subsec}_{camera}{increment_string}.{self.extension.lower()}"
        except AttributeError:
            destination_filename = f"{pathlib.Path(self.source).name}"

        full_destination = pathlib.Path(f"{self.destination_base}/{destination_dir}/{destination_filename}")

        return full_destination

    def _parse_datetime(self):
        datetime_raw = None
        try:
            # EXIF
            for key in ["EXIF:DateTimeOriginal", "EXIF:ModifyDate"]:
                if key in self.raw_metadata:
                    datetime_raw = self.raw_metadata[key]
                    self.log.debug(f"{self.source}:datetime_raw: {datetime_raw}")
            if not datetime_raw:
                raise KeyError
            try:
                subsec_raw = self.raw_metadata["EXIF:SubSecTimeOriginal"]
                self.log.debug(f"{self.source}:subsec_raw: {subsec_raw}")
            except KeyError:
                subsec_raw = 0
            self.log.debug(f"{self.source}:parse_datetime: using EXIF")
            timestamp = f"{datetime_raw} {subsec_raw}"
            image_datetime = datetime.strptime(timestamp, "%Y:%m:%d %X %f")
        except KeyError:
            try:
                # QuickTime (MOV)
                datetime_raw = self.raw_metadata["QuickTime:CreateDate"]
                self.log.debug(f"{self.source}:datetime_raw: {datetime_raw}")
                subsec_raw = 0
                self.log.debug(f"{self.source}:parse_datetime: [{self.source}] using Quicktime metadata")
                timestamp = f"{datetime_raw} {subsec_raw}"
                image_datetime = datetime.strptime(timestamp, "%Y:%m:%d %X %f")
            except KeyError:
                # No metadata info so have to derive datetime...

                # Determine if filename has something that looks like a date and/or
                # time in the name
                contains_datetime = DATE_PATTERN.search(str(self.source))
                if contains_datetime:
                    self.log.debug(f"{self.source}:parse_datetime: using datetime from filename")
                    self.log.debug(f"{self.source}:contains_datetime == {contains_datetime}")
                    self.log.debug(f"{self.source}:datetime_group_0 == {contains_datetime.group(0)}")
                    if contains_datetime.group(1):
                        timestamp = contains_datetime.group(1)
                    if contains_datetime.group(3):
                        timestamp = f"{timestamp}-{contains_datetime.group(3)}"
                    if contains_datetime.group(5):
                        timestamp = f"{timestamp}.{contains_datetime.group(5)}"

                    try:
                        image_datetime = dateutil.parser.parse(timestamp)
                    except dateutil.parser._parser.ParserError:
                        self.log.warn("Unable to parse date from filename.")
                        image_datetime = None
                else:
                    # Use ctime from stat
                    self.log.debug(f"{self.source}:parse_datetime: using ctime")
                    image_datetime = datetime.fromtimestamp(self.source_stat.st_ctime)

        self.log.debug(f"image_datetime: {image_datetime}")

        if self.time_delta:
            image_datetime = image_datetime + self.time_delta
        if self.day_delta:
            image_datetime = image_datetime + self.day_delta

        return image_datetime

    def _parse_make(self, make=""):
        if str(make):
            make = make.split()[0].capitalize()
            if make == "Htc":
                make = "HTC"
            elif make == "Lg":
                make = "LG"
            elif make == "Research":
                make = None
        else:
            make = None

        self.log.debug(f"Parsed make as: {make}")
        return make

    def _get_metadata(self):
        metadata = None

        try:
            with exiftool.ExifToolHelper() as et:
                metadata = et.get_metadata(str(self.source))
        except FileNotFoundError:
            raise ExifNotFound("exiftool not found")

        self.log.debug(f"metadata: {json.dumps(metadata, sort_keys=True,indent=2)}")

        return metadata[0]

    def _parse_model(self, make="", model=""):
        # Remove make from the model
        if make:
            model = model.replace(make, "").strip()
            model = model.replace(make.upper(), "").strip()

        if " " in model:
            model = "".join([word.capitalize() for word in model.split(" ")])

        if model == "":
            model = None

        self.log.debug(f"Parsed model as: {model}")

        return model

    def is_valid_extension(self):
        if self.extension.lower() in self.supported_extensions:
            return True
        else:
            return False

    def is_raw(self):
        if self.extension.lower() in RAW_EXTENSIONS:
            return True
        else:
            return False

    def parse_metadata(self):
        self.source_stat = os.stat(self.source)
        self.raw_metadata = self._get_metadata()

        # Gather the pieces we need
        self.datetime = self._parse_datetime()
        try:
            self.make = self._parse_make(self.raw_metadata["EXIF:Make"])
        except KeyError:
            try:
                self.make = self._parse_make(self.raw_metadata["MakerNotes:Make"])
            except KeyError:
                self.make = "Unknown"

        try:
            self.model = self._parse_model(self.make, self.raw_metadata["EXIF:Model"])
        except KeyError:
            try:
                self.model = self._parse_model(self.make, self.raw_metadata["MakerNotes:Model"])
            except KeyError:
                self.model = ""

        self.sha256 = self._calculate_sha256(str(self.source))

        increment = 0
        self.destination = self._generate_destination()

        self.destination_dir = self.destination.parent

        try:
            self.destination_stat = os.stat(self.destination)
        except FileNotFoundError:
            self.destination_sha256 = None
        else:
            self.destination_sha256 = self._calculate_sha256(str(self.destination))
            self.log.debug(f"SHA256: {self.sha256} -> SHA256: {self.destination_sha256}")
            while self.destination.exists() and self.destination_sha256 != self.sha256:
                increment += 1
                self.destination = self._generate_destination(increment=increment)
                try:
                    self.destination_stat = os.stat(self.destination)
                    self.destination_sha256 = self._calculate_sha256(str(self.destination))
                except FileNotFoundError:
                    self.destination_sha256 = None
                self.log.debug(f"SHA256: {self.sha256} -> SHA256: {self.destination_sha256}")

    def perform(self):
        self.destination_dir.mkdir(parents=True, exist_ok=True)

        if self.is_move:
            safe_move(self.source, self.destination)
        else:
            safe_copy(self.source, self.destination)

        with exiftool.ExifToolHelper() as et:
            et.set_tags(
                self.destination,
                tags={
                    "EXIF:DateTimeOriginal": self.datetime,
                    "EXIF:CreateDate": self.datetime,
                    "EXIF:ModifyDate": self.datetime,
                },
            )



def safe_copy(src, dst):
    """Copy a file in an atomic manner.
    Adapted from https://alexwlchan.net/2019/03/atomic-cross-filesystem-moves-in-python/

    Arguments:
        * src: Source filename
        * dst: Destination filename
    """

    destination_dir = dst.parent

    copy_id = uuid.uuid4()
    tmp_dst = f"{destination_dir}/.{copy_id}.tmp"
    try:
        shutil.copyfile(src, tmp_dst)
        os.rename(tmp_dst, dst)
    except OSError:
        os.unlink(tmp_dst)
        raise


def safe_move(src, dst):
    """Move a file in an atomic manner.
    Adapted from https://alexwlchan.net/2019/03/atomic-cross-filesystem-moves-in-python/

    Arguments:
        * src: Source filename
        * dst: Destination filename
    """

    try:
        os.rename(src, dst)
    except OSError as err:
        # Filesystem boundary error
        if err.errno == errno.EXDEV:
            copy_id = uuid.uuid4()
            destination_dir = dst.parent
            tmp_dst = f"{destination_dir}/.{copy_id}.tmp"
            try:
                shutil.copyfile(src, tmp_dst)
                os.rename(tmp_dst, dst)
                os.unlink(src)
            except OSError:
                os.unlink(tmp_dst)
                raise
        else:
            # Re-raise the OSError
            raise


def calculate_time_delta(time_delta: str) -> timedelta:
    delta_parts = time_delta.split(":")
    negate = False
    if delta_parts[0][0] == "-":
        negate = True

    hours = int(delta_parts[0])
    minutes = int(delta_parts[1])
    seconds = int(delta_parts[2])

    delta = timedelta(
        hours=hours,
        minutes=-(minutes) if negate else minutes,
        seconds=-(seconds) if negate else seconds,
    )

    return delta


def calculate_day_delta(day_delta: str) -> timedelta:
    delta = timedelta(
        days=int(day_delta),
    )

    return delta
