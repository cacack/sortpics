import argparse
import json
import multiprocessing
import logging
import os
import pathlib
import sys

import sortpics.rename

from .__version__ import __title__, __version__

NCORE = multiprocessing.cpu_count()

log = logging.getLogger(__title__)

def main():
    args = parse_args()
    #log = logging.getLogger(__title__)

    queue_size = args.thread_count * 2
    log.debug(f"threads: {queue_size}")

    # Create worker pool and add jobs
    queue = multiprocessing.Queue(maxsize=queue_size)
    iolock = multiprocessing.Lock()
    pool = multiprocessing.Pool(args.thread_count, initializer=process, initargs=(queue, iolock))

    if args.old_naming:
        args.precision = 2

    try:
        for source_dir in args.source_dirs:
            for source_file in walk(source_dir, recursive=args.recursive):
                todo = sortpics.rename.ImageRename(
                    source_file,
                    args.destination_dir,
                    move=args.move,
                    precision=args.precision,
                    raw_path=args.raw_path,
                    dry_run=args.dry_run,
                    old_naming=args.old_naming,
                    time_adjust=args.time_adjust,
                    day_adjust=args.day_adjust,
                )
                queue.put(todo)
                with iolock:
                    log.debug(f"Queued: {source_file.is_file()} {source_file}")
        for _ in range(args.thread_count):
            queue.put(None)
        pool.close()
        pool.join()
    except KeyboardInterrupt:
        log.info("Ctrl-C pressed.  Terminating..")
        pool.terminate()

    if args.move and args.clean and not args.dry_run:
        for source_dir in args.source_dirs:
            for dirpath, dirnames, filenames in os.walk(source_dir, topdown=False):
                try:
                    os.rmdir(dirpath)
                    log.info(f"Cleaned up {dirpath}")
                except OSError:
                    log.warn(f"{dirpath} does not exist or is not empty")


# Do the actual work...
def process(queue, iolock):
    log = logging.getLogger(__title__)
    #print("in process..")
    while True:
        try:
            work = queue.get()
            if work is None:
                break
            if work.is_valid_extension():
                with iolock:
                    log.info(f"processing: {work.source}")
                try:
                    work.parse_metadata()
                    log.info(f"metadata: {work.raw_metadata}")
                    #print(json.dumps(work.raw_metadata,indent=4,default=str))
                except FileNotFoundError:
                    with iolock:
                        log.warn(f"FileNotFound: {work.source} <- {work.source_unresolved}")
                except sortpics.rename.ExifNotFound:
                    with iolock:
                        log.error("exiftool not found.  Please install first.")
                        sys.exit(255)
                else:
                    if work.dry_run:
                        adj = "I would "
                    else:
                        adj = ""
                    if work.is_move:
                        verb = f"{adj}Move"
                    else:
                        verb = f"{adj}Copy"
                    with iolock:
                        print(f"{verb}: {work.source} -> {work.destination}")
                    if not work.dry_run:
                        work.perform()
        except KeyboardInterrupt:
            return


def walk(source, pattern="*", only_file=True, recursive=False):
    """list all files in pth matching a given pattern, can also list dirs if only_file is False"""
    source = pathlib.Path(str(source))
    log = logging.getLogger(__title__)
    log.debug(f"walk:source == {source}")
    if source.match(pattern) and not (only_file and source.is_dir()):
        log.debug(f"walk:here1")
        yield source
    else:
        log.debug(f"walk:here3")
        for sub in source.iterdir():
            if sub.is_dir() and recursive:
                yield from walk(sub, pattern, only_file, recursive)
            else:
                if sub.match(pattern):
                    yield sub


def parse_args():
    parser = argparse.ArgumentParser(
        prog=__title__,
        description="Sorts pictures",
    )
    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version="{} {}".format(__title__, __version__),
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "-c",
        "--copy",
        dest="move",
        action="store_false",
        help="copy files",
    )
    group.add_argument(
        "-m",
        "--move",
        action="store_true",
        help="move files",
    )
    parser.add_argument(
        "source_dirs",
        metavar="SRC",
        nargs="+",
        type=str,
        help="source directory",
    )
    parser.add_argument(
        "-t",
        "--threads",
        dest="thread_count",
        type=int,
        default=NCORE,
        help="max number of threads",
    )
    parser.add_argument(
        "-p",
        "--precision",
        type=int,
        default=6,
        help="microsecond precision",
    )
    parser.add_argument(
        "-R",
        "--raw-path",
        type=str,
        help="place RAW files here",
    )
    parser.add_argument(
        "--time-adjust",
        type=str,
        help="Time difference",
    )
    parser.add_argument(
        "--day-adjust",
        type=str,
        help="Day difference",
    )
    parser.add_argument(
        "-r",
        "--recursive",
        action="store_true",
        default=False,
        help="recursively process directories",
    )
    parser.add_argument(
        "-C",
        "--clean",
        action="store_true",
        default=False,
        help="clean up (remove) other files",
    )
    parser.add_argument(
        "destination_dir",
        metavar="DEST",
        type=str,
        help="destination directory",
    )
    parser.add_argument(
        "--pretend",
        "--dry-run",
        dest="dry_run",
        action="store_true",
        default=False,
        help="don't actually do the move or copy",
    )
    parser.add_argument(
        "--old-naming",
        action="store_true",
        default=False,
        help="don't actually do the move or copy",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="increase output verbosity",
    )

    args = parser.parse_args()

    # Define the logging levels we use, with the first being the default
    levels = [
        # logging.ERROR,  # default
        logging.WARNING,
        logging.INFO,
        logging.DEBUG,
    ]
    verbosity = levels[
        # Derive verbosity level given argument but cap it based on levels
        max(min(len(levels) - 1, args.verbose), 0)
    ]
    logging.basicConfig(level=verbosity)

    log = logging.getLogger(__title__)
    log.debug(f"log level: {verbosity}")

    return args


if __name__ == "__main__":
    sys.exit(main())
