from datetime import datetime, timedelta
import dateutil
import pytest
import pytest_mock
import os

from sortpics.rename import ImageRename, calculate_time_delta


@pytest.mark.parametrize(
    "make, expected",
    [
        ("NIKON CORPORATION", "Nikon"),
        ("Canon", "Canon"),
        ("LG Electronics", "Lg"),
        ("KODAK Zi8 Pocket Video Camera", "Kodak"),
        ("", None),
    ],
)
def test_parse_make(make, expected):
    assert ImageRename._parse_make(object, make) == expected


@pytest.mark.parametrize(
    "make, model, expected",
    [
        ("Nikon", "NIKON D5100", "D5100"),
        ("Canon", "Canon EOS DIGITAL REBEL", "EosDigitalRebel"),
        ("Canon", "Canon PowerShot S410", "PowershotS410"),  # Not sure I like this
        ("Motorola", "Droid", "Droid"),
        ("Sony", "DSC-W330", "DSC-W330"),
        ("Lg", "VS740", "VS740"),
        ("Kodak", "KODAK Zi8 Pocket Video Camera", "Zi8PocketVideoCamera"),
        ("", "", None),
    ],
)
def test_parse_model(make, model, expected):
    assert ImageRename._parse_model(object, make, model) == expected

@pytest.mark.parametrize(
    "string, delta",
    [
        ("00:00:01", timedelta(seconds=1)),
        ("00:01:00", timedelta(minutes=1)),
        ("01:00:00", timedelta(hours=1)),
        ("-03:00:05", timedelta(hours=-3,seconds=-5)),
        ("01:02:03", timedelta(hours=1,minutes=2,seconds=3)),
    ]
)
def test_calculate_time_delta(string, delta):
    assert calculate_time_delta(string) == delta

def test__parse_datetime(mocker):
    datetime_string = "2000:01:01 11:59:59"
    subsec = "100000"
    datetime_sans_subsec_expected = datetime.strptime(datetime_string, "%Y:%m:%d %X")
    datetime_subsec_expected = datetime.strptime(f"{datetime_string} {subsec}", "%Y:%m:%d %X %f")

    mock_date = "20010401"
    mock_datetime = f"{mock_date}-162000"
    mock_datetime_subsec = f"{mock_datetime}.04"
    mock_filename_date = f"{mock_date}.jpg"
    mock_filename_datetime = f"{mock_datetime}.jpg"
    mock_filename_datetime_subsec = f"{mock_datetime_subsec}.jpg"
    date_filename_expected = dateutil.parser.parse(mock_date)
    datetime_filename_expected = dateutil.parser.parse(mock_datetime)
    datetime_subsec_filename_expected = dateutil.parser.parse(mock_datetime_subsec)

    mock_filename_dummy = "dummy.jpg"
    mock_stat = os.stat_result
    mock_stat.st_ctime = int(datetime_sans_subsec_expected.strftime("%s"))

    uut = ImageRename("foo","bar")

    # Sans subsec
    uut.raw_metadata = {"EXIF:DateTimeOriginal": datetime_string}
    actual = uut._parse_datetime()
    assert actual == datetime_sans_subsec_expected

    # With subsec
    uut.raw_metadata = {
        "EXIF:DateTimeOriginal": datetime_string,
        "EXIF:SubSecTimeOriginal": subsec
    }
    actual = uut._parse_datetime()
    assert actual == datetime_subsec_expected

    # Without EXIF

    ## From filename
    uut.raw_metadata = {}
    uut.source = mock_filename_date
    actual = uut._parse_datetime()
    assert actual == date_filename_expected

    uut.source = mock_filename_datetime
    actual = uut._parse_datetime()
    assert actual == datetime_filename_expected

    uut.source = mock_filename_datetime_subsec
    actual = uut._parse_datetime()
    assert actual == datetime_subsec_filename_expected

    ## From stat's ctime
    uut.source = mock_filename_dummy
    uut.source_stat = mock_stat
    actual = uut._parse_datetime()
    assert actual == datetime_sans_subsec_expected
